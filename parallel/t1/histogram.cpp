#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

constexpr int nbuckets = 2048;

void seq_histogram(int* array, int n, int histogram[nbuckets]) {
	//reset buckets
	for (int i = 0; i < nbuckets; ++i)
		histogram[i] = 0;
	//populate buckets
	for (int i = 0; i < n; ++i)
		++histogram[array[i]];
}

void par_histogram(int* array, int n, int* histogram,int index,int ncores) {
	//reset buckets
	int id = omp_get_thread_num();
	for (int i = 0; i < nbuckets; ++i)
		histogram[index*id+i] = 0;
	//populate buckets
	#pragma omp for
	for (int i = 0; i < n; ++i)
		++histogram[array[i]+index*id];
}

bool check(int n, int histogram[nbuckets])
{
	int sum = 0;
	for (int i = 0; i < nbuckets; ++i)
		sum += histogram[i];
	return sum == n;
}

int main() {

	//init input
	const int n = 33554432;
	int* arr = new int[n];
	int ncores = 3;
	int mem_size = 64;
	int mem_blocks;
	int index,i;
	int* par;

	mem_blocks = nbuckets/64;
	mem_blocks = mem_blocks*64;
	if(nbuckets%64 != 0){
		mem_blocks+=64;
	}
	mem_blocks = mem_blocks*ncores;
	par = (int*)malloc(mem_blocks*sizeof(int));
	if(par == NULL){
		return -1;
	}
	index = mem_blocks/ncores;

	for (i = 0; i < n; ++i)
		arr[i] = rand() % nbuckets;

	int histogram[nbuckets];
	int histogram2[nbuckets];

	printf("number of buckets = %d\n", nbuckets);
	printf("number of items = %d\n", n);

	double t,t2;

	t = omp_get_wtime();
	seq_histogram(arr, n, histogram);
	t = omp_get_wtime() - t;
	t2 = omp_get_wtime();
	#pragma omp parallel num_threads(ncores)
	{
		par_histogram(arr, n, par,index,ncores);
	}
	for (int i = 0; i < nbuckets; ++i){
		histogram2[i] = par[i];
		for(int j = 1; j < ncores; ++j){
			histogram2[i] += par[i+index*j];
		}
	}
	t2 = omp_get_wtime() - t2;
	printf("seq_histogram, elapsed time = %.3f seconds, check passed = %c\n", t, check(n, histogram) ? 'Y' : 'N');
	printf("par_histogram, elapsed time = %.3f seconds, check passed = %c\n", t2, check(n, histogram2) ? 'Y' : 'N');
	printf("speed boost of: %lf\n",(double)t/t2);

	free(par);
	delete[] arr;
	//exit
	return EXIT_SUCCESS;
}
