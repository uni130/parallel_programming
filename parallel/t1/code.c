#include <omp.h>
#include <math.h>
#include <stdio.h>
double makesum(int id,int max,int operations){
	double aux = 0;
	int piece = operations/max;
	int start = id*piece;
	int j;
	for(j = start;j < start+piece;j++){
		aux += 1.0/operations*(sqrt(1-(((double)j/operations)*((double)j/operations))));
	}
	if(id == 0){
		for(j = 1;j <= operations%max;j++){
			aux += 1.0/operations*(sqrt(1-(((double)(operations-j)/operations)*((double)(operations-j)/operations))));
		}
	}
	return aux;
}
int main(){
	long int n;
	double sol = 0;
	int i,j;
	double sol2 = 0;
	int ncores;
	double time1;
	double time2;
	double time3;
	double time4;
	
	for(n = 10000000;n > 1;n = n/10){
		/*sequential*/
		time1 = omp_get_wtime();
		sol = 0;
		for(i = 0;i < n;i++){
			sol += 1.0/n*(sqrt(1-(((double)i/n)*((double)i/n))));
		}
		sol = sol*4;
		time2 = omp_get_wtime();
		time3 = time2-time1;
		printf("sequential %lf\t time: %lf\n",sol,time3);
		for(ncores = 3;ncores > 0;ncores--){
			/*parallel*/
			sol2 = 0;
			time1 = omp_get_wtime();
			#pragma omp parallel num_threads(ncores)
			{
				#pragma omp atomic
				sol2+=makesum(omp_get_thread_num(),omp_get_num_threads(),n);
			}
			sol2 = sol2*4;
			time2 = omp_get_wtime();
			time4 = time2-time1;
			printf("parallel %d operations %d cores: %lf\t time: %lf\t serial/parallel = %lf\n",n,ncores,sol2,time4,time3/time4);
		}
	}
	return 0;
}
