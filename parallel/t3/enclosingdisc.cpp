#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#include <omp.h>

struct point2D {
	point2D() {}
	point2D(float x, float y) : x(x), y(y) {}
	float x, y;
};

struct mesh2D {
	mesh2D(const int n) : n(n) {
		arr = (point2D*)malloc(n*sizeof(point2D));
		for(int i=0; i<n; ++i)
			arr[i].x = rand()/(RAND_MAX+0.1f),
			arr[i].y = rand()/(RAND_MAX+0.1f);
	}
	~mesh2D() {
		free(arr);
	}
	point2D *arr = nullptr;
	const int n = 0;
};

static inline float square_distance(const point2D &a, const point2D &b) { return (a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y); }

static void find_min_max(point2D &min, point2D &max, const mesh2D &mesh) {
	//init variables
	max.x = min.x = mesh.arr[0].x;
	max.y = min.y = mesh.arr[0].y;
	for(int i=1; i<mesh.n; ++i) 
		min.x = mesh.arr[i].x<min.x ? mesh.arr[i].x : min.x,
		max.x = mesh.arr[i].x>max.x ? mesh.arr[i].x : max.x,
		min.y = mesh.arr[i].y<min.y ? mesh.arr[i].y : min.y,
		max.y = mesh.arr[i].y>max.y ? mesh.arr[i].y : max.y;
}

static void find_min_max_par(point2D &min, point2D &max, const mesh2D &mesh, int ncores) {
	//init variables
	point2D max_aux;
	point2D min_aux;
	max.x = min.x = mesh.arr[0].x;
	max.y = min.y = mesh.arr[0].y;
	#pragma omp parallel num_threads(ncores) private(max_aux,min_aux)
	{
		max_aux.x = min_aux.x = mesh.arr[0].x;
		max_aux.y = min_aux.y = mesh.arr[0].y;
		int index = omp_get_thread_num();
		for(int i=1; i<mesh.n*index/ncores; ++i) 
			min_aux.x = mesh.arr[i].x<min_aux.x ? mesh.arr[i].x : min_aux.x,
			max_aux.x = mesh.arr[i].x>max_aux.x ? mesh.arr[i].x : max_aux.x,
			min_aux.y = mesh.arr[i].y<min_aux.y ? mesh.arr[i].y : min_aux.y,
			max_aux.y = mesh.arr[i].y>max_aux.y ? mesh.arr[i].y : max_aux.y;
		#pragma omp critical
		{
			min.x = min_aux.x<min.x ? min_aux.x : min.x;
			max.x = max_aux.x>max.x ? max_aux.x : max.x;
			min.y = min_aux.y<min.y ? min_aux.y : min.y;
			max.y = max_aux.y>max.y ? max_aux.y : max.y;
		}
	}
}

static int farthest_point(const mesh2D &mesh, const point2D c) {
	float sqdst, max = 0;
	int argmax = 0;
	for(int i=0; i<mesh.n; ++i) {
		sqdst = square_distance(mesh.arr[i], c);
		if(sqdst>max)
			argmax = i,
			max = sqdst;
	}
	return argmax;
}

int main(int argc, char *argv[]) {
	const int n = argc>1 ? atoi(argv[1]) : 16000000;
	assert(n>0 && n%8==0);
	srand(123456);
	mesh2D mesh(n);
	point2D min, max, c;
	int ncores = 4;
	float r;
	
	//sequantial
	clock_t t = clock();
	find_min_max(min, max, mesh);
	c.x = (max.x-min.x)*0.5+min.x;
	c.y = (max.y-min.y)*0.5+min.x;
	int argmax = farthest_point(mesh, c);
	r = sqrtf(square_distance(c,mesh.arr[argmax]));
	t = clock()-t;

	//parallel
	clock_t t2 = clock();
	find_min_max_par(min, max, mesh,ncores);
	c.x = (max.x-min.x)*0.5+min.x;
	c.y = (max.y-min.y)*0.5+min.x;
	argmax = farthest_point(mesh, c);
	r = sqrtf(square_distance(c,mesh.arr[argmax]));
	t2 = clock()-t2;

	printf("sequential:\n center : (%.2f,%.2f)\nradius : %.4f\nelapsed time : %.3f sec\n", c.x, c.y, r, (double)t/CLOCKS_PER_SEC);
	
	printf("parallel:\n center : (%.2f,%.2f)\nradius : %.4f\nelapsed time : %.3f sec\n", c.x, c.y, r, (double)t2/CLOCKS_PER_SEC);
	
	return 0;
}
