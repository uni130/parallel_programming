#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#include <immintrin.h>

struct point2D {
	point2D() {}
	point2D(float x, float y) : x(x), y(y) {}
	float x, y;
};

struct mesh2D {
	mesh2D(const int n) : n(n) {
		arr = (point2D*)malloc(n * sizeof(point2D));
		for (int i = 0; i < n; ++i)
			arr[i].x = rand() / (RAND_MAX + 0.1f),
			arr[i].y = rand() / (RAND_MAX + 0.1f);
	}
	~mesh2D() {
		free(arr);
	}
	point2D* arr = nullptr;
	const int n = 0;
};

struct mesh_par {
	mesh_par(const int n) :n(n) {
		x = (float*)malloc(n * sizeof(float));
		y = (float*)malloc(n * sizeof(float));
		for (int i = 0; i < n; ++i) {
			x[i] = rand() / (RAND_MAX + 0.1f);
			y[i] = rand() / (RAND_MAX + 0.1f);
		}
	}
	~mesh_par() {
		free(x);
		free(y);
	}
	float* x = nullptr;
	float* y = nullptr;
	const int n = 0;
};

static float max_distance_par(mesh_par& mesh,float &c_x,float &c_y,int n) {
	__m256 tmp;
	__m256 aux = _mm256_set1_ps(-1);
	__m256 cx = _mm256_set1_ps(c_x);
	__m256 cy = _mm256_set1_ps(c_y);
	for (int i = 0; i < n; i += 8) {
		__m256 x = _mm256_set_ps(mesh.x[i + 7], mesh.x[i + 6], mesh.x[i + 5], mesh.x[i + 4], mesh.x[i + 3], mesh.x[i + 2], mesh.x[i + 1], mesh.x[i]);
		__m256 y = _mm256_set_ps(mesh.y[i + 7], mesh.y[i + 6], mesh.y[i + 5], mesh.y[i + 4], mesh.y[i + 3], mesh.y[i + 2], mesh.y[i + 1], mesh.y[i]);
		__m256 dx = _mm256_sub_ps(x, cx);
		dx = _mm256_mul_ps(dx, dx);
		__m256 dy = _mm256_sub_ps(y, cy);
		dy = _mm256_mul_ps(dy, dy);
		__m256 d = _mm256_add_ps(dx, dy);
		aux = _mm256_max_ps(aux, d);
	}

	aux = _mm256_sqrt_ps(aux);
	tmp = _mm256_permute2f128_ps(aux, aux, 0b10000001);
	aux = _mm256_max_ps(aux, tmp);
	tmp = _mm256_shuffle_ps(aux, aux, 0b00001110);
	aux = _mm256_max_ps(aux, tmp);
	tmp = _mm256_shuffle_ps(aux, aux, 0b00000001);
	aux = _mm256_max_ps(aux, tmp);

	return _mm256_cvtss_f32(aux);
}
static inline float square_distance(const point2D& a, const point2D& b) { return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y); }
static void find_min_max_par(float& x_min,float& x_max, float& y_min, float& y_max,mesh_par& mesh,int n) {
	__m256 tmp, aux;
	// get x min
	aux = _mm256_load_ps(mesh.x);
	for (int i = 8; i < n; i += 8)
		aux = _mm256_min_ps(aux, _mm256_load_ps(mesh.x + i));
	tmp = _mm256_permute2f128_ps(aux, aux, 0b10000001);
	aux = _mm256_min_ps(aux, tmp);
	tmp = _mm256_shuffle_ps(aux, aux, 0b00001110);
	aux = _mm256_min_ps(aux, tmp);
	tmp = _mm256_shuffle_ps(aux, aux, 0b00000001);
	aux = _mm256_min_ps(aux, tmp);
	x_min= _mm256_cvtss_f32(aux);

	//get y min
	aux = _mm256_load_ps(mesh.y);
	for (int i = 8; i < n; i += 8)
		aux = _mm256_min_ps(aux, _mm256_load_ps(mesh.y + i));
	tmp = _mm256_permute2f128_ps(aux, aux, 0b10000001);
	aux = _mm256_min_ps(aux, tmp);
	tmp = _mm256_shuffle_ps(aux, aux, 0b00001110);
	aux = _mm256_min_ps(aux, tmp);
	tmp = _mm256_shuffle_ps(aux, aux, 0b00000001);
	aux = _mm256_min_ps(aux, tmp);
	y_min = _mm256_cvtss_f32(aux);

	//get x max
	aux = _mm256_load_ps(mesh.x);
	for (int i = 8; i < n; i += 8)
		aux = _mm256_max_ps(aux, _mm256_load_ps(mesh.x + i));
	tmp = _mm256_permute2f128_ps(aux, aux, 0b10000001);
	aux = _mm256_max_ps(aux, tmp);
	tmp = _mm256_shuffle_ps(aux, aux, 0b00001110);
	aux = _mm256_max_ps(aux, tmp);
	tmp = _mm256_shuffle_ps(aux, aux, 0b00000001);
	aux = _mm256_max_ps(aux, tmp);
	x_max = _mm256_cvtss_f32(aux);

	//get y max
	aux = _mm256_load_ps(mesh.y);
	for (int i = 8; i < n; i += 8)
		aux = _mm256_max_ps(aux, _mm256_load_ps(mesh.y + i));
	tmp = _mm256_permute2f128_ps(aux, aux, 0b10000001);
	aux = _mm256_max_ps(aux, tmp);
	tmp = _mm256_shuffle_ps(aux, aux, 0b00001110);
	aux = _mm256_max_ps(aux, tmp);
	tmp = _mm256_shuffle_ps(aux, aux, 0b00000001);
	aux = _mm256_max_ps(aux, tmp);
	y_max = _mm256_cvtss_f32(aux);
}
static void find_min_max(point2D& min, point2D& max, const mesh2D& mesh) {
	//init variables
	max.x = min.x = mesh.arr[0].x;
	max.y = min.y = mesh.arr[0].y;
	for (int i = 1; i < mesh.n; ++i)
		min.x = mesh.arr[i].x<min.x ? mesh.arr[i].x : min.x,
		max.x = mesh.arr[i].x>max.x ? mesh.arr[i].x : max.x,
		min.y = mesh.arr[i].y<min.y ? mesh.arr[i].y : min.y,
		max.y = mesh.arr[i].y>max.y ? mesh.arr[i].y : max.y;
}

static int farthest_point(const mesh2D& mesh, const point2D c) {
	float sqdst, max = 0;
	int argmax = 0;
	for (int i = 0; i < mesh.n; ++i) {
		sqdst = square_distance(mesh.arr[i], c);
		if (sqdst > max)
			argmax = i,
			max = sqdst;
	}
	return argmax;
}

int main(int argc, char* argv[]) {
	const int n = 67108864;// argc > 1 ? atoi(argv[1]) : 16000000;

	assert(n > 0 && n % 8 == 0);
	srand(123456);
	mesh2D mesh(n);
	point2D min, max, c;
	float r;
	clock_t t = clock();
	find_min_max(min, max, mesh);
	c.x = (max.x - min.x) * 0.5 + min.x;
	c.y = (max.y - min.y) * 0.5 + min.x;
	int argmax = farthest_point(mesh, c);
	r = sqrtf(square_distance(c, mesh.arr[argmax]));
	t = clock() - t;
	auto serial = t;
	printf("CPU >>> center : (%.2f,%.2f)  radius : %.4f  elapsed time : %.3f sec\n", c.x, c.y, r, (double)t / CLOCKS_PER_SEC);
	////////////////////////////////////////
	mesh_par pmesh(n);
	float x_min, x_max, y_min, y_max,c_x,c_y;
	t = clock();
	find_min_max_par(x_min, x_max, y_min, y_max, pmesh, n);
	c_x = (x_min + x_max) /2;
	c_y = (y_min + y_max) /2;
	float max_dist = max_distance_par(pmesh, c_x, c_y, n);
	t = clock() - t;
	auto parallel = t;
	printf("GPU >>> center : (%.2f,%.2f)  radius : %.4f  elapsed time : %.3f sec\n", c_x, c_y, max_dist, (double)t / CLOCKS_PER_SEC);
	printf("Speed up : %.4f\n", (double)serial /(double) parallel);
	return 0;
}
