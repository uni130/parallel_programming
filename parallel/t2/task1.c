#include <mpi.h>
#include <stdio.h>

int main(int argc, char **argv) {
	MPI_Init(&argc, &argv);
	int rank, size, message=0;

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	int count=1;
	printf("Processor with rank %d out of %d processors : barrier reached!\n", rank, size);
	////////////////////////////////////////////////////////////////////////////////////////
	//Since we need to make 2 circles around the processes which means we need the /////////
	//complexity is O(2n)=O(n)//////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////
	
	if(rank==0){
		MPI_Send(&message,count,MPI_INT,1,0,MPI_COMM_WORLD);
		MPI_Recv(&message,count,MPI_INT,size-1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		MPI_Send(&message,count,MPI_INT,1,0,MPI_COMM_WORLD);
		MPI_Recv(&message,count,MPI_INT,size-1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
	}
	else if(rank==size-1){
		MPI_Recv(&message,count,MPI_INT,rank-1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		MPI_Send(&message,count,MPI_INT,0,0,MPI_COMM_WORLD);
		MPI_Recv(&message,count,MPI_INT,rank-1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		MPI_Send(&message,count,MPI_INT,0,0,MPI_COMM_WORLD);
	}
	else{
		MPI_Recv(&message,count,MPI_INT,rank-1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		MPI_Send(&message,count,MPI_INT,rank+1,0,MPI_COMM_WORLD);
		MPI_Recv(&message,count,MPI_INT,rank-1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		MPI_Send(&message,count,MPI_INT,rank+1,0,MPI_COMM_WORLD);
	}

	////////////////////////////////////////////////////////////////////////////////////////
	printf("Processor with rank %d out of %d processors : barrier passed!\n", rank, size);
	MPI_Finalize();
	return 0;
}
