	#pragma once
	#include <mpi.h>
	#include <stdio.h>
	#include <unistd.h> //usleep
	#define NDEBUG //comment to enable asserts
	#include <assert.h>
	#define TAG 42
	#define STREAM_ELEMENT_SIZE 10
	#define STREAM_LENGTH 100
	inline static void sleep_milliseconds(int ms) { assert(ms>=0 && ms<1000); 
	usleep(ms*1000); }
	#define T_f0 3
	#define T_f1 4
	#define T_g1 6
	#define T_f2 1
	static int f0(int n) { sleep_milliseconds(T_f0); return n+1;}
	static int f1(int n) { sleep_milliseconds(T_f1); return n*2;}
	static int g1(int n) { sleep_milliseconds(T_g1); return n/2;}
	static int f2(int n) { sleep_milliseconds(T_f2); return n*n;}

	void mySend(int array[],int count, int dst){
		 MPI_Send(array, count, MPI_INT, dst, TAG, MPI_COMM_WORLD);
	}

	void myRecv(int array[],int count, int src){
		MPI_Recv(array, count, MPI_INT, src, TAG, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
	}

	void myRecv_any(int array[],int count) { 
		MPI_Recv(array, count, MPI_INT, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE); 
		//return status.MPI_SOURCE; 
	}

	void M_first_2( int size){

		int count=STREAM_ELEMENT_SIZE/(size-2);
		int i;
		int x[STREAM_ELEMENT_SIZE+1]={0};
		int y[count+1]={0};
		for(i=0; i<STREAM_LENGTH; ++i) {
			for(int j=0; j<STREAM_ELEMENT_SIZE; ++j)
				x[j] = f0(j);
			for(int j=0;j<size-2;++j){
				
				for(int k=0;k<count;++k){
					y[k]=x[count*j+k];
				}
				mySend(y,count+1,j+1); // for ranks: 1..N-2 (i%(size-2)+1)			
			}
		}	
		y[count]=1;
		//set and send terminated
		for(int j=0;j<size-2;++j){
			for(int k=0;k<count;++k){
				y[k]=x[count*j+k];
			}
			mySend(y,count+1,j+1); // for ranks: 1..N-2 (i%(size-2)+1)			
		}

	}
void M_2( int size){
	int count=STREAM_ELEMENT_SIZE/(size-2);
	int i;
	//int x[STREAM_ELEMENT_SIZE+1]={0};
	int y[count+1]={0};
	
	while(true) {
		myRecv(y,count+1,0);
		if(y[count]==1) {
			mySend(y,count+1,size-1);
			return;
		}
		for(int j=0; j<count; ++j)
			y[j] = g1(f1(y[j]));
		mySend(y,count+1,size-1);
	}
}
void M_last_2( int size){
	int count=STREAM_ELEMENT_SIZE/(size-2);
	int i;
	int y[count+1];
	int cnt=0;
	while(true) {
		myRecv_any(y,count+1);
		if(y[count]==1){
			cnt++;
			if(cnt==size-2)
			return;	
		}
	
		for(int j=0; j<count; ++j)
			y[j] = f2(y[j]);
		//x.printme(++msg_counter);
		//fflush(stdout);
	}

}
	static double get_time(MPI_Comm comm) {
	MPI_Barrier(comm);
	return MPI_Wtime();
	MPI_Barrier(comm);
	}

	#define REQUIRED_COMM_SIZE 4
	int main(int argc, char** argv) {
		MPI_Init(&argc, &argv);
		int rank, size;
		MPI_Comm_size(MPI_COMM_WORLD, &size);
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		if(size==REQUIRED_COMM_SIZE) {	 	
			for(int i=0;i<10;i++){
				double start = get_time(MPI_COMM_WORLD);
				
				if(rank==0) {
					M_first_2(size);
				}
				if(rank!=0 && rank!=size-1) {
					M_2(size);	
				}
			
				if(rank==size-1){
					M_last_2(size);
				} 

				double end = get_time(MPI_COMM_WORLD);
				if(rank==0) printf("elapsed time = %.2f seconds\n", end-start);
			}
		} 
		else {
			if(rank==0) printf("run with option '-n %d'\n", REQUIRED_COMM_SIZE);
		}
		MPI_Finalize(); 
		return 0;
	}
