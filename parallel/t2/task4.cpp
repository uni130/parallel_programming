#include "common.hpp"
static int calc_cores(){
	int M = (T_g1+T_f1)*STREAM_ELEMENT_SIZE*STREAM_LENGTH;
	int M_first = T_f0*STREAM_LENGTH*STREAM_ELEMENT_SIZE;
	return M/M_first+1;
}
void M_first_2(const int size){
		streamelement x;
		int i;
		for(i=0; i<STREAM_LENGTH; ++i) {
			for(int j=0; j<STREAM_ELEMENT_SIZE; ++j)
				x[j] = f0(j);
			x.send(i%(size-2)+1); // for ranks: 1..N-2
		}
		x.set_terminated();
		for(int j=0;j<size-2;++j){
			x.send((i+j)%(size-2)+1); 
		}

}
void M_2(const int size){
		streamelement x;
		while(true) {
			x.recv(0);
			if(x.is_terminated()) {
				x.send(size-1);
				return;
			}
			#pragma omp parallel for schedule(dynamic,calc_cores())
			for(int j=0; j<STREAM_ELEMENT_SIZE; ++j)
				x[j] = g1(f1(x[j]));
			x.send(size-1);
		}
}
void M_last_2(const int size){
	streamelement x;
	int msg_counter = 0;
	int counter=0;
	while(true) {
		x.recv_any();
		if(x.is_terminated()){
			counter++;
			if(counter==size-2)
			return;	
		}
	
		for(int j=0; j<STREAM_ELEMENT_SIZE; ++j)
			x[j] = f2(x[j]);
		//x.printme(++msg_counter);
		//fflush(stdout);
	}

}

	#define REQUIRED_COMM_SIZE 4
	int main(int argc, char** argv) {
		MPI_Init(&argc, &argv);
		int rank, size;
		MPI_Comm_size(MPI_COMM_WORLD, &size);
		if(size>=REQUIRED_COMM_SIZE) {
		MPI_Comm_rank(MPI_COMM_WORLD, &rank); 
		double start = get_time(MPI_COMM_WORLD);
		if(rank==0) M_first_2(size);
		if(rank!=0 && rank!=size-1) M_2(size);
		if(rank==size-1) M_last_2(size);
		double end = get_time(MPI_COMM_WORLD);
		if(rank==0) printf("elapsed time = %.2f seconds\n", end-start);
		} else {
		if(rank==0) printf("run with option '-n %d'\n", REQUIRED_COMM_SIZE);
		}
		MPI_Finalize(); 
		return 0;
}
