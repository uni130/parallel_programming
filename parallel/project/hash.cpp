
#include <immintrin.h>
#include<math.h>
#include<iostream>
#include <chrono>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
using namespace std;

////[VARIABLE_DECLARATION_AND_MEM_ALLOC___START]////

unsigned int h0 = 0x6a09e667;
unsigned int h1 = 0xbb67ae85;
unsigned int h2 = 0x3c6ef372;
unsigned int h3 = 0xa54ff53a;
unsigned int h4 = 0x510e527f;
unsigned int h5 = 0x9b05688c;
unsigned int h6 = 0x1f83d9ab;
unsigned int h7 = 0x5be0cd19;

unsigned int s1[8] = { 0x6a09e68e , 0xcc09ad2d  , 0x816acb4d , 0xeee9a6f0 , 0x2837ffaa , 0x48263496 , 0x0df3a6e8 , 0xf0ba2654 };
unsigned int s2[8] = { 0xe8b5a1fe , 0xfbf23d03  , 0x343b5ab1 , 0xf9fcdd37 , 0x1826b672 , 0xabfb6833 , 0x5ca465ba , 0xb2bb9906 };
unsigned int s3[8] = { 0x783aa9b1 , 0xe56dc4cb  , 0x8586b12e , 0xb4ab0d51 , 0xaccb1816 , 0xb42c0bc5 , 0xd220ff80 , 0xee403201 };
unsigned int s4[8] = { 0xe97df89e , 0x3fe26114  , 0xcbd73b46 , 0x06015762 , 0x7d19336f , 0x10899117 , 0x89c7c578 , 0xa4e5ee25 };

unsigned int perm[8]{ 1,2,3,4,5,6,7,0 };
__m256i* salt1_alloc = (__m256i*) memalign(256, 256);
__m256i* salt2_alloc = (__m256i*) memalign(256, 256);
__m256i* salt3_alloc = (__m256i*) memalign(256, 256);
__m256i* salt4_alloc = (__m256i*) memalign(256, 256);
__m256i* permute_alloc = (__m256i*) memalign(256, 256);

////[VARIABLE_DECLARATION_AND_MEM_ALLOC___START]////

char* serial_hash(string input_password) {

	int length = input_password.length();
	char* src = (char*)malloc(32);
	for (int i = 0; i < length; i++) {
		src[i] = input_password[i];
	}
	//[length<8_START]/////////////////this section is used if the password is shorter than 8 characters
	if (length == 1) {
		src[1] = src[0] ^ h0;
		length++;
	}
	if (length == 2) {
		src[2] = src[1] ^ h1;
		length++;
	}
	if (length == 3) {
		src[3] = src[2] ^ h2;
		length++;
	}
	if (length == 4) {
		src[4] = src[3] ^ h3;
		length++;
	}
	if (length == 5) {
		src[5] = src[4] ^ h4;
		length++;
	}
	if (length == 6) {
		src[6] = src[5] ^ h5;
		length++;
	}
	if (length == 7) {
		src[7] = src[6] ^ h6;
		length++;
	}
	//[length<8_END]/////////////////
	for (int i = length; i < 32; i++) {
		src[i] ^= (src[i - 3] ^ src[i - 8] ^ src[i - 5] ^ src[i - 1]);
	}

	unsigned int h[8];
	h[0] = h0;
	h[1] = h1;
	h[2] = h2;
	h[3] = h3;
	h[4] = h4;
	h[5] = h5;
	h[6] = h6;
	h[7] = h7;

	unsigned int tmp;
	unsigned int f;
	unsigned int k;
	//////////[START_MIX]/////////////
	for (int i = 0; i < 32; i++) {
		if (i < 20) {
			f = (h[1] & h[2]) | (~(h[2]) & h[3]);
			k = 0x5A827999;
		}
		else if (i < 40) {
			f = (h[1]^h[2]^h[3]);
			k = 0x6ED9EBA1;
		}
		else if (i < 60) {
			f = (h[7] & h[2]) | (h[7] & h[3]) | (h[7] & h[5]);
			k = 0x8F1BBCDC;
		}
		else {
			f = (h[5] ^ h[6] ^ h[0]) | (h[1] ^ h[2] ^ h[3]) ;
			k = 0xCA62C1D6;
		}
		tmp = (h[0] << 5) + (h[1] >> 2) + (h[6] >> 2) + (h[4] >> 6) + f + k + (unsigned int)src[i];
		h[7] = h[6];
		h[6] = h[5];
		h[5] = (h[4] >> 6); 
		h[4] = h[3];
		h[3] = (h[2] >> 19) + f + h[1];
		h[2] = (h[1] >> 29) + k + k + f;
		h[1] = h[0];
		h[0] = tmp;
	}
	//////////[END_MIX]/////////////

	//////////[START_MIX]/////////////
	for (int i = 0; i < 48; i++) {
		if (i < 12) {

			for (int j = 0; j < 8; j++)
				h[j] ^= s1[j];

			for (int j = 0; j < 8; j++)
				h[j] ^= s2[j];
		}
		else if (i < 24) {

			for (int j = 0; j < 8; j++)
				h[j] ^= s2[j];

			for (int j = 0; j < 8; j++)
				h[j] ^= s3[j];
		}
		else if (i < 36) {

			for (int j = 0; j < 8; j++)
				h[j] ^= s3[j];

			for (int j = 0; j < 8; j++)
				h[j] ^= s4[j];
		}
		else {

			for (int j = 0; j < 8; j++)
				h[j] ^= s4[j];

			for (int j = 0; j < 8; j++)
				h[j] ^= s1[j];
		}
		unsigned int tmp = h[0];
		for (int i = 0; i < 7; i++)
			h[i] = h[i + 1];
		h[7] = tmp;
	}
	//////////[END_MIX]/////////////

	h[0] += h0;
	h[1] += h1;
	h[2] += h2;
	h[3] += h3;
	h[4] += h4;
	h[5] += h5;
	h[6] += h6;
	h[7] += h7;

	memcpy(src, h, 32);
	return src;
}

char* parallel_hash(string input_password) {
	int length = input_password.length();
	char* src = (char*)malloc(32);
	for (int i = 0; i < length; i++) {
		src[i] = input_password[i];
	}
	//[length<8_START]/////////////////this section is used if the password is shorter than 8 characters
	if (length == 1) {
		src[1] = src[0] ^ h0;
		length++;
	}
	if (length == 2) {
		src[2] = src[1] ^ h1;
		length++;
	}
	if (length == 3) {
		src[3] = src[2] ^ h2;
		length++;
	}
	if (length == 4) {
		src[4] = src[3] ^ h3;
		length++;
	}
	if (length == 5) {
		src[5] = src[4] ^ h4;
		length++;
	}
	if (length == 6) {
		src[6] = src[5] ^ h5;
		length++;
	}
	if (length == 7) {
		src[7] = src[6] ^ h6;
		length++;
	}
	//[length<8_END]/////////////////
	for (int i = length; i < 32; i++) {
		src[i] ^= (src[i - 3] ^ src[i - 8] ^ src[i - 5] ^ src[i - 1]);
	}

	unsigned int h[8];
	h[0] = h0;
	h[1] = h1;
	h[2] = h2;
	h[3] = h3;
	h[4] = h4;
	h[5] = h5;
	h[6] = h6;
	h[7] = h7;

	unsigned int tmp;
	unsigned int f;
	unsigned int k;
	//////////[START_MIX]/////////////
	for (int i = 0; i < 32; i++) {
		if (i < 20) {
			f = (h[1] & h[2]) | (~(h[2]) & h[3]);
			k = 0x5A827999;
		}
		else if (i < 40) {
			f = (h[1] ^ h[2] ^ h[3]);
			k = 0x6ED9EBA1;
		}
		else if (i < 60) {
			f = (h[7] & h[2]) | (h[7] & h[3]) | (h[7] & h[5]);
			k = 0x8F1BBCDC;
		}
		else {
			f = (h[5] ^ h[6] ^ h[0]) | (h[1] ^ h[2] ^ h[3]);
			k = 0xCA62C1D6;
		}
		tmp = (h[0] << 5) + (h[1] >> 2) + (h[6] >> 2) + (h[4] >> 6) + f + k + (unsigned int)src[i];
		h[7] = h[6];
		h[6] = h[5];
		h[5] = (h[4] >> 6); 
		h[4] = h[3];
		h[3] = (h[2] >> 19) + f + h[1];
		h[2] = (h[1] >> 29) + k + k + f;
		h[1] = h[0];
		h[0] = tmp;
	}
	//////////[END_MIX]/////////////
	__m256i* mem = (__m256i*) memalign(256, 256);
	memcpy(mem, h, 32);
	__m256i arr = _mm256_load_si256(mem);

	////////[VARIABLES_MAGIJA]///////////

	memcpy(salt1_alloc, s1, 32);
	memcpy(salt2_alloc, s2, 32);
	memcpy(salt3_alloc, s3, 32);
	memcpy(salt4_alloc, s4, 32);
	memcpy(permute_alloc, perm, 32);
	__m256i salt1 = _mm256_load_si256(salt1_alloc);
	__m256i salt2 = _mm256_load_si256(salt2_alloc);
	__m256i salt3 = _mm256_load_si256(salt3_alloc);
	__m256i salt4 = _mm256_load_si256(salt4_alloc);
	__m256i permute = _mm256_load_si256(permute_alloc);
	//////////[START_MIX]/////////////
	for (int i = 0; i < 48; i++) {
		if (i < 12) {
			arr = _mm256_xor_si256(arr, salt1);
			arr = _mm256_xor_si256(arr, salt2);
		}
		else if (i < 24) {
			arr = _mm256_xor_si256(arr, salt2);
			arr = _mm256_xor_si256(arr, salt3);
		}
		else if (i < 36) {
			arr = _mm256_xor_si256(arr, salt3);
			arr = _mm256_xor_si256(arr, salt4);
		}
		else {
			arr = _mm256_xor_si256(arr, salt4);
			arr = _mm256_xor_si256(arr, salt1);
		}
		arr=_mm256_permutevar8x32_epi32(arr, permute);
	}
	//////////[END_MIX]/////////////
	_mm256_store_si256(mem, arr);
	memcpy(h, mem, 32);
	/////////////////////////////////////
	h[0] += h0;
	h[1] += h1;
	h[2] += h2;
	h[3] += h3;
	h[4] += h4;
	h[5] += h5;
	h[6] += h6;
	h[7] += h7;

	memcpy(src, h, 32);
	return src;
}

bool hash_compare(char* h1, char* h2) {

	for (int i = 0; i < 32; i++) {
		if (h1[i] != h2[i])
			return false;
	}

	return true;
}

int main() {


	string password[4] = { "s3cretpassword","secretpassword","s3cretpasswore","s3cretpassworf" };
	
	const int num = 30000; //NUMBER OF COMPARISONS
	string passwords[num];	//random password storage
	static const char alphanum[] ="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"; //THIS IF JUST FOR GENERATION
	const int len = 5; //LENGTH OF passoword with escape token '\0'
	char s[len];

	char* hash = serial_hash("s3cretpassword"); //parallel_hash("s3cretpassword");
	
	//Generating random password in a bad way _ START
	for (int j = 0; j < num-1; j++) {
		for (int i = 0; i < len-1; ++i) {
			s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
		}
		s[len - 1] = '\0';
		passwords[j] = string(s);
	}
	passwords[num-1] = "s3cretpassword";
	//Generating random password in a bad way _ END

	/////////////////////////[SERIAL_TESTING_START]////////////////////////////////
	auto start_time = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < num; i++) {
		if (hash_compare(hash,serial_hash(passwords[i]))) {
			cout << "PASSWORD CRACK SUCCESSFUL, RESULT: " << passwords[i] << endl;
		}
	}
	auto end_time = std::chrono::high_resolution_clock::now();
	auto time = end_time - start_time;
	cout << "Serial result = " <<(time).count()/ CLOCKS_PER_SEC <<"ms"<< endl;
	/////////////////////////[SERIAL_TESTING_END]////////////////////////////////

	/////////////////////////[PARALLEL_TESTING_START]////////////////////////////////
	start_time = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < num; i++) {
		if (hash_compare(hash, parallel_hash(passwords[i]))) {
			cout << "PASSWORD CRACK SUCCESSFUL, RESULT: " << passwords[i] << endl;
		}
	}
	end_time = std::chrono::high_resolution_clock::now();
	time = end_time - start_time;
	std::cout  << "Parallel result = " << (time).count()/CLOCKS_PER_SEC<<"ms"<< endl;
	/////////////////////////[SERIAL_TESTING_START]////////////////////////////////
	
	return 0;
}

