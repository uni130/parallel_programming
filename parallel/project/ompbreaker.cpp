#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <omp.h>
#include "hash.hpp"
#define MAX_LENGTH 20
#define PCHARS 64
#define PCHARS_AVX PCHARS/8


char* parallel_decode(int length,char* coded,char* values,int ncores,int this_thread){
	int i;
	char* selected = NULL;
	char* hashed = NULL;
	int* selected_int = NULL;
	char* chain = NULL;
	int* chain_int = NULL;
	int subchain_size = PCHARS/ncores;
	int start = subchain_size*this_thread;
	int end;
	if(this_thread == ncores-1){
		end = PCHARS;
	}else{
		end = subchain_size*(this_thread+1);
	}

	/*variable setting start*/
	chain = (char*)malloc(1+MAX_LENGTH*sizeof(char));
	if(!chain){
		return NULL;
	}
	chain_int = (int*)malloc(length*sizeof(int));
	if(!chain_int){
		free(chain);
		return NULL;
	}

	

	for(i = 0;i < (length-1);i++){
		chain[i] = values[0];
		chain_int[i] = 0;
	}
	chain_int[length-1] = start;
	chain[length-1] = values[start];
	chain[length] = '\0';
	selected_int = &chain_int[length-1];
	selected = &chain[length-1];
	/*variable setting end*/
	
	printf("entering decoding loop\n");

	/*decoding loop*/
	while(1){
		printf("making compraisson\n");
		hashed = serial_hash(chain);
		if(strcmp(coded, hashed)==0){
			break;
		}
		printf("chain: %s\nhashed password: %s\nhashed chain: %s\n",chain,coded,hashed);
		free(hashed);
		//delay only existant for debugging reasons
		usleep(100000);
		(*selected_int)++;
	
		if(*selected_int == end){
			if(length == 1){
				free(chain_int);
				free(chain);
				return NULL;
			}
			while(*selected_int == PCHARS||(*selected_int == end && selected == &chain[length-1])){
				if(selected == &chain[length-1]){
					*selected_int = start;
					*selected = values[start];
				}else{
					*selected_int = 0;
					*selected = values[0];
				}
				selected_int--;
				selected--;
				(*selected_int)++;
				if(*selected_int<PCHARS){
					*selected = values[*selected_int];
				}
				
				if(selected == chain && *selected_int == PCHARS){
					free(chain_int);
					free(chain);
					return NULL;
				}
			}
			selected_int = &chain_int[length-1];
			selected = &chain[length-1];
		}else{
			*selected = values[*selected_int];
		}
	}
	free(chain_int);
	return chain;
}

int main(){
	int size = 4;
	int i;
	char* aux_sol;
	int length_count = 1;
	char* pswrd = NULL;
	char* coded = NULL;
	char* solution = NULL;
	char accepted_values[PCHARS]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','.',' '};
	 

	pswrd = (char*)malloc(1+MAX_LENGTH*sizeof(char));
	strcpy(pswrd,"a3");
	if(!pswrd){
		return -1;
	}
	
	coded = serial_hash(pswrd);

	
	printf("the password: %s has been encoded and now it is: %s\n",pswrd,coded);


	/*parallel break*/
	#pragma omp parallel num_threads(size) private(aux_sol)
	{
		aux_sol = parallel_decode(length_count,coded,accepted_values,size,omp_get_thread_num());
		if(aux_sol){
			solution = aux_sol;
		}
	}

	printf("first check didnt crush\n");
	while(solution == NULL){
		length_count++;
		#pragma omp parallel num_threads(size) private(aux_sol)
		{
			aux_sol = parallel_decode(length_count,coded,accepted_values,size,omp_get_thread_num());
			if(aux_sol){
				solution = aux_sol;
			}
		}
		
		if(length_count == MAX_LENGTH){
			free(pswrd);
			free(coded);
			return -1;
		}
	}
	if(solution){
		if(strcmp(solution,pswrd) == 0){
			printf("the solution is %s\n", solution);
		}
		free(solution);
	}
	free(pswrd);
	free(coded);
	return 0;


}
