#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hash.hpp"
#define MAX_LENGTH 20
#define PCHARS 64
char* serial_decode(int length,char* coded,char* values){
	int i;
	char* selected = NULL;
	int* selected_int;
	char* chain;
	int* chain_int;
	
	/*variable setting start*/
	chain = (char*)malloc(length*sizeof(char)+1);
	if(!chain){
		return NULL;
	}
	chain_int = (int*)malloc(length*sizeof(int));
	if(!chain_int){
		free(chain);
		return NULL;
	}
	
	for(i = 0;i < length;i++){
		chain[i] = values[0];
		chain_int[i] = 0;
	}
	chain[length] = '\0';
	selected_int = &chain_int[length-1];
	selected = &chain[length-1];
	/*variable setting end*/
	
	/*decoding loop*/
	while(1){
		if(hash_compare(coded, serial_hash(chain))){
			break;
		}
		(*selected_int)++;
	
		if(*selected_int == PCHARS){
			if(length == 1){
				free(chain_int);
				free(chain);
				return NULL;
			}
			while(*selected_int == PCHARS){
				*selected_int = 0;
				*selected = values[0];
				selected_int--;
				selected--;
				(*selected_int)++;
				if(*selected_int!=PCHARS){
					*selected = values[*selected_int];
				}
				
				if(selected == chain && *selected_int == PCHARS){
					free(chain_int);
					free(chain);
					return NULL;
				}
			}
			selected_int = &chain_int[length-1];
			selected = &chain[length-1];
		}else{
			*selected = values[*selected_int];
		}
	}
	free(chain_int);
	return chain;
}

int main(){
	double diff = 0.0;
	time_t start;
	time_t stop;
	int length_count = 1;
	char* pswrd = NULL;
	char* coded = NULL;
	char* solution = NULL;
	char accepted_values[PCHARS]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','.',' '};
	pswrd = (char*)malloc(1+MAX_LENGTH*sizeof(char));
	if(!pswrd){
		return -1;
	}
	coded = (char*)malloc(32);
	if(!coded){
		free(pswrd);
		return -1;
	}
	printf("enter the pasword to test\n");
	strcpy(pswrd,"p");

	coded = parallel_hash(pswrd);

	printf("the password: %s \n has been encoded and now it is: %s\n",pswrd,coded);

	start = clock();
	solution = serial_decode(length_count,coded,accepted_values);
	while(solution == NULL){
		length_count++;
		solution = serial_decode(length_count,coded,accepted_values);
		if(length_count == MAX_LENGTH){
			free(pswrd);
			free(coded);
			return -1;
		}
	}
	stop = clock();
	if(strcmp(solution,pswrd)!=0){
		printf("unsuccessful password break\n");
		free(solution);
		free(pswrd);
		free(coded);
		return -1;
	}else{
		diff = difftime(stop, start);
		printf("the solution is %s and was taken in %lf seconds", solution,diff/1000000.0000);
		free(solution);
		free(pswrd);
		free(coded);
		return 0;
	}
}
