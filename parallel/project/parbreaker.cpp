#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "hash.hpp"
#include "common.hpp"
#define MAX_LENGTH 20
#define PCHARS 64
#define PCHARS_AVX PCHARS/8


char* parallel_decode(int length,char* coded,char* values,int ncores,int this_thread){
	int i;
	char* selected = NULL;
	int* selected_int = NULL;
	char* chain = NULL;
	int* chain_int = NULL;
	int subchain_size = PCHARS/ncores;
	int start = subchain_size*this_thread;
	int end;
	streamelement x;
	if(this_thread == ncores-1){
		end = PCHARS;
	}else{
		end = subchain_size*(this_thread+1);
	}

	/*variable setting start*/
	chain = (char*)malloc(length*sizeof(char)+1);
	if(!chain){
		return NULL;
	}
	chain_int = (int*)malloc(length*sizeof(int));
	if(!chain_int){
		free(chain);
		return NULL;
	}

	

	for(i = 0;i < (length-1);i++){
		chain[i] = values[0];
		chain_int[i] = 0;
	}
	chain_int[length-1] = start;
	chain[length-1] = values[start];
	chain[length] = '\0';
	selected_int = &chain_int[length-1];
	selected = &chain[length-1];
	/*variable setting end*/
	

	/*decoding loop*/
	while(1){
		if(this_thread != (ncores-1)){
			x.send(this_thread+1);
		}else{
			x.send(0);
		}
		if(x.is_terminated()) {
			free(chain_int);
			return chain;
		}
		
		if(hash_compare(coded, parallel_hash(chain))){
			x.set_terminated();
			if(this_thread != ncores-1){
				x.send(this_thread+1);
			}else{
				x.send(0);
			}
			break;
		}
		(*selected_int)++;
	
		if(*selected_int == end){
			if(length == 1){
				free(chain_int);
				free(chain);
				return NULL;
			}
			while(*selected_int == PCHARS||(*selected_int == end && selected == &chain[length-1])){
				if(selected == &chain[length-1]){
					*selected_int = start;
					*selected = values[start];
				}else{
					*selected_int = 0;
					*selected = values[0];
				}
				selected_int--;
				selected--;
				(*selected_int)++;
				if(*selected_int<PCHARS){
					*selected = values[*selected_int];
				}
				
				if(selected == chain && *selected_int == PCHARS){
					free(chain_int);
					free(chain);
					return NULL;
				}
			}
			selected_int = &chain_int[length-1];
			selected = &chain[length-1];
		}else{
			*selected = values[*selected_int];
		}
		if(this_thread != 0){
			x.recv(this_thread-1);
		}else{
			x.recv(ncores-1);
		}
	}
	free(chain_int);
	return chain;
}

int main(int argc, char** argv){
	MPI_Init(&argc, &argv);
	int rank, size;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	int i;
	int length_count = 3;
	char* pswrd = NULL;
	char* coded = NULL;
	char** sol_array = NULL;
	char* solution = NULL;
	char accepted_values[PCHARS]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','.',' '};
	 

	pswrd = (char*)malloc(1+MAX_LENGTH*sizeof(char));
	strcpy(pswrd,"p");
	if(!pswrd){
		MPI_Finalize();
		return -1;
	}
	coded = (char*)malloc(32);
	if(!coded){
		free(pswrd);
		MPI_Finalize();
		return -1;
	}
			
	coded = parallel_hash(pswrd);

	if(rank == 0){
		printf("the password: %s has been encoded and now it is: %s\n",pswrd,coded);
	}


	/*parallel break*/
	MPI_Barrier(MPI_COMM_WORLD);
	double start = MPI_Wtime();
	solution = parallel_decode(length_count,coded,accepted_values,size,rank);
		
	MPI_Barrier(MPI_COMM_WORLD);
	while(solution == NULL){
		length_count++;
		solution = parallel_decode(length_count,coded,accepted_values,size,rank);
		MPI_Barrier(MPI_COMM_WORLD);
		if(length_count == MAX_LENGTH){
			MPI_Finalize();
			free(pswrd);
			free(coded);
			return -1;
		}
	}
	double end = MPI_Wtime();
	if(solution){
		if(strcmp(solution,pswrd) == 0){
			printf("the solution is %s\n", solution);
			printf("the time it took was %lf\n", end-start);
		}
		free(solution);
	}
	free(pswrd);
	free(coded);
	MPI_Finalize();
	return 0;


}
